//  velocity.cpp
//  MoonLander
//  author: Zerui Tang.

#include "velocity.h"

// Default constructer and set all data to default 0.
Velocity::Velocity(){
    setDx(0.0);
    setDy(0.0);
}

// Set all data to values provided by the velocity object.
Velocity::Velocity(const float dx,const float dy){
    setDx(dx);
    setDy(dy);
}
