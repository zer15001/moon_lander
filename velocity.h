//  velocity.h
//  MoonLander
//  author: Zerui Tang.

#ifndef velocity_h
#define velocity_h

class Velocity{
private:
    float dx;
    float dy;
public:
    Velocity();
    Velocity(const float dx, const float dy);
    float getDx() const { return dx; }
    float getDy() const { return dy; }
    void setDx(const float dx) { this->dx = dx; }
    void setDy(const float dy) { this->dy = dy; }
};
#endif /* velocity_h */
