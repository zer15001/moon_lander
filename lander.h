//  lander.h
//  MoonLander
//  author: Zerui Tang.


#ifndef lander_h
#define lander_h

#include "velocity.h"
#include "point.h"
#include "uiDraw.h"

class Lander{
private:
    Point loc;
    Velocity vel;
    int fuel;
    bool alive;
    bool landed;
    
public:
    Lander();
    Point getPoint();
    Velocity getVelocity();
    
    int getFuel() const { return fuel; }
    bool isAlive() const { return alive; }
    bool isLanded() const { return landed; }
   
    void setLanded(const bool landed) { this->landed = landed; }
    void setAlive(const bool alive){ this->alive = alive; }
    void setFuel(int fuel){ this->fuel = fuel; }
    
    
    bool canThrust();
    void applyGravity(float);
    void applyThrustLeft();
    void applyThrustRight();
    void applyThrustBottom();
    void advance();
    void draw() const;
};

#endif /* lander_h */
