//  lander.cpp
//  MoonLander
//  author: Zerui Tang.

#include "lander.h"

// constructor
Lander::Lander()
{
    vel.setDx(0.0);
    vel.setDy(0.0);
    loc.setX(0);
    loc.setY(0);
    setAlive(true);
    setLanded(false);
    setFuel(0);
}

Point Lander :: getPoint(){
    return loc;
}

Velocity Lander::getVelocity(){
    return vel;
}

// fuel needs to be over 0.
bool Lander::canThrust(){
    return fuel > 0;
}

// Apply gravity to the ship.
void Lander::applyGravity(float gravity)
{
    vel.setDy(vel.getDy() - gravity);
}

// Apply ship direction left
void Lander::applyThrustLeft()
{
    if(!canThrust()) return;
    vel.setDx(1);
    fuel --;
}

// Apply ship direction right.
void Lander::applyThrustRight()
{
    if(!canThrust()) return;
    vel.setDx(-1);
    fuel --;
}

// Apply ship direction bottom.
void Lander::applyThrustBottom()
{
    if(!canThrust()) return;
    vel.setDy(1);
    fuel --;
}

// Advance the ship.
void Lander::advance()
{
    loc.addX(vel.getDx());
    loc.addY(vel.getDy());
}

// Draw the ship to the interface.
void Lander::draw() const
{
    drawLander(loc);
}

